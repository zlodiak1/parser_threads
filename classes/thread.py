from threading import Thread
 
 
class MyThread(Thread): 
    def __init__(self, name, saver, parser):
        Thread.__init__(self)
        self.name = name
        self.saver = saver
        self.parser = parser
    
    def run(self):
        print('start thread with nme: '+self.name)
        content = self.parser.get_content(self.name)
        self.saver.save(self.name, content)
