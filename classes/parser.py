import requests
from bs4 import BeautifulSoup

class Parser:
    def __init__(self, url, headers):
        self.url = url
        self.headers = headers

    def get_content(self, search_phrase):
        root_html = self.get_html(self.url+search_phrase)
        links = self.get_links(root_html)
        content = self.get_content_inner(links)
        return content

    def get_html(self, url):
        html = requests.get(url, headers=self.headers).text
        return html

    def get_links(self, html):
        links = []

        soup = BeautifulSoup(html, 'lxml')
        links_obj = soup.find('div', 'vacancy-serp').findAll('a', {'class': 'HH-LinkModifier'})

        for link in links_obj:
            links.append(link.get('href'))

        return links

    def get_content_inner(self, links):
        content = []
        for link in links:
            html = self.get_html(link)

            soup = BeautifulSoup(html, 'lxml')
            company =       soup.find('span', {'itemprop': 'name'})
            position =      soup.find('h1', {'itemprop': 'title'}),
            link_ =         link

            try:
                company_ = company[0].getText().strip()
            except Exception:
                company_ = company.text.strip()


            try:
                position_ = position[0].getText().strip()
            except Exception:
                position_ = position.text.strip()

            content_inner = {
                'company':  company_,
                'position': position_,
                'link':     link_
            }

            content.append(content_inner)

        return content
                      