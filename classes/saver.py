import os


class Saver:
    def save(self, phrase, content):
        with open('results/'+phrase+'.txt', 'w', encoding='utf-8') as f:
            f.write(phrase.upper() + '\n')
            for item in content:  
                for val in item.values():              
                    f.write(str(val) + ' :: ')   
                f.write('\n')

    def clear(self, search_phrases):
        for phrase in search_phrases:
            file = 'results/'+phrase+'.txt'
            if os.path.isfile(file):
                os.remove(file)        